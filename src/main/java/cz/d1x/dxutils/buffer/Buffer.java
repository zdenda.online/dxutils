package cz.d1x.dxutils.buffer;

import cz.d1x.dxutils.buffer.memory.MemoryBuffer;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * Buffer is component that usually stands on top of some other storage to which it flushes the data.
 * In the data organization, it is similar to the map data structure as it stores values per key.
 * </p>
 * <p>
 * Flush of the data can be driven by data itself (depending on {@link Bufferable#invokesFlush()}), manually by invoking
 * any of flush methods (it can flush only specific or all keys) or automatically if you use any of wrappers
 * (e.g. {@link BufferAutoFlush} that flushes once per given period of time).
 * If flush fails (e.g. throws exception or other cases), the data will remain in the buffer (in the original order).
 * </p>
 * <p>
 * The buffer ensures ordering of the values as they were put into the buffer for a single key.
 * If you attempt to store values of the same key concurrently, ordering is not predictable for these
 * two puts but the underlying implementations usually should do "their best" to be fair.
 * </p>
 * <p>
 * Thread-safety of the buffer is guaranteed by the {@link #getLockMode()}.
 * Some implementations may support some/all modes (typically accept lock mode in constructor).
 * Some implementations return single value that signals their thread-safety.
 * </p>
 * <p>
 * For optimized performance, flush operations may not release all resources held for given key. To release all
 * resources for a key, you must one invoke one of remove/clear methods.
 * </p>
 *
 * @param <K> key type generic
 * @param <V> value type generic
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 * @see Bufferable
 * @see BufferAutoFlush
 * @see MemoryBuffer
 * @see LockMode
 */
public interface Buffer<K, V extends Bufferable> {

    /**
     * Gets lock mode of buffer. See {@link LockMode} for more information.
     * Some implementations support multiple/all lock mode. Some operate in pre-defined mode that cannot be set.
     *
     * @return actual lock mode
     */
    LockMode getLockMode();

    /**
     * Puts single or multiple values in the buffer.
     * Ordering of values will be preserved.
     *
     * @param key    key under which to put
     * @param values values to be put
     */
    @SuppressWarnings("unchecked")
    void put(K key, V... values);

    /**
     * Puts values in the buffer.
     * Ordering of values will be preserved according to iterator of given collection.
     *
     * @param key    key under which to put
     * @param values values to be put
     */
    void put(K key, Collection<V> values);

    /**
     * Gets all currently stored (not removed) keys.
     * This method also returns keys that may not have any values buffered at the moment (because they may have
     * been flushed) but were not removed yet.
     *
     * @return actual set of buffered keys
     */
    Set<K> keys();

    /**
     * Gets count of currently buffered values per given keys.
     * This operation is thread safe (see a javadoc if the interface for more information).
     *
     * @return count of buffered values for every key.
     */
    Map<K, Integer> size();

    /**
     * Gets buffered values of given key.
     * It gives a copy of buffered values thus any changes to returned list won't affect internal state of the buffer.
     *
     * @param key key which keys should be returned
     * @return buffered values of the key or null if given key is not present or was removed
     */
    List<V> valuesOf(K key);

    /**
     * Gets a flag whether the buffer is clear.
     * Buffer is clear when resources for all previously put keys were removed (by any of remove/clear methods).
     * When buffer is flushed for all keys (there are no values), it is not guaranteed it will be clear (is implementation
     * specific).
     *
     * @return true if buffer is clear, otherwise false
     */
    boolean isClear();

    /**
     * Flushes immediately all values of all keys that are buffered.
     * If flush of any key fails, values of that key will be kept in the buffer (and this method will return false).
     * It does not releases all resources, if you want to completely clear it, call one of remove methods instead.
     *
     * @return true if all keys were successfully flushed, otherwise false
     */
    boolean flush();

    /**
     * Flushes single or multiple given keys that are buffered.
     * If flush of any key fails, values of that key will be kept in the buffer (and this method will return false).
     * It does not releases resources of the key, if you want to completely clear it, call one of remove methods instead.
     *
     * @param keys keys to be flushed
     * @return true if all given keys were successfully flushed, otherwise false
     */
    @SuppressWarnings("unchecked")
    boolean flush(K... keys);

    /**
     * Removes all keys from the buffer.
     * If there are any data for any removed key, it gets flushed immediately before its removal.
     * If flush of any key fails, values of that key will be kept in the buffer (and this method will return false).
     * In case you don't want to flush before removal, use {@link #clearWithoutFlush()} instead.
     *
     * @return true if all keys were successfully cleared (and flushed), otherwise false
     */
    boolean clear();

    /**
     * Removes all keys from the buffer.
     * If there are any data for any removed key, it will NOT be flushed and will be returned instead.
     * In case you want to flush before removal, use {@link #clear()} instead.
     *
     * @return all currently buffered values that were removed (and not flushed)
     */
    Map<K, List<V>> clearWithoutFlush();

    /**
     * Removes single or multiple given keys from the buffer.
     * If there are any data for any removed key, it gets flushed immediately before its removal.
     * If flush of any key fails, values of that key will be kept in the buffer (and this method will return false).
     * In case you don't want to flush before removal, use {@link #removeWithoutFlush(Object[])} instead.
     *
     * @param keys keys to be removed
     * @return true if all keys were successfully removed (and flushed), otherwise false
     */
    @SuppressWarnings("unchecked")
    boolean remove(K... keys);

    /**
     * Removes single or multiple given keys from the buffer.
     * If there are any data for any removed key, it will NOT be flushed and will be returned instead.
     * In case you want to flush before removal, use {@link #remove(Object[])} instead.
     *
     * @param keys keys to be removed
     * @return all currently buffered values that were removed (and not flushed)
     */
    @SuppressWarnings("unchecked")
    Map<K, List<V>> removeWithoutFlush(K... keys);
}
