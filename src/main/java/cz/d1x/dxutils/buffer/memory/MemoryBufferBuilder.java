package cz.d1x.dxutils.buffer.memory;

import cz.d1x.dxutils.buffer.BufferAutoFlush;
import cz.d1x.dxutils.buffer.Bufferable;
import cz.d1x.dxutils.buffer.KeyFlushStrategy;
import cz.d1x.dxutils.buffer.LockMode;

/**
 * A builder for creation of {@link MemoryBuffer} instances.
 *
 * @see MemoryBuffer
 */
public class MemoryBufferBuilder<K, V extends Bufferable> {

    private final KeyFlushStrategy<K, V> flushStrategy;
    private LockMode lockMode = LockMode.LOCK_BUFFER;
    private String name = null; // calls super.toString if not set
    private boolean autoRemove = false;
    private boolean flushOnEveryPut = false;

    /**
     * Creates a new builder with given flushing strategy.
     *
     * @param flushStrategy flushing strategy
     * @throws IllegalArgumentException possible exception if flush strategy is null
     */
    public MemoryBufferBuilder(KeyFlushStrategy<K, V> flushStrategy) {
        if (flushStrategy == null) throw new IllegalArgumentException("Flush strategy cannot be null");
        this.flushStrategy = flushStrategy;
    }

    /**
     * Sets how buffer should be locked during operations with it.
     * See {@link LockMode} for more information.
     *
     * @param lockMode mode of lock
     * @return this builder
     */
    public MemoryBufferBuilder<K, V> withLockMode(LockMode lockMode) {
        this.lockMode = lockMode;
        return this;
    }

    /**
     * Sets a name of this buffer that is returned in {@link #toString()} method.
     * It may be useful to name your buffer as {@link BufferAutoFlush} uses {@link #toString()} to name flushing
     * thread.
     *
     * @param name name of the buffer
     * @return this builder
     */
    public MemoryBufferBuilder<K, V> withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Sets whether this buffer should also automatically remove key during flush (defaults to false).
     * That practically means that clients does not need to use any of remove/clear methods and stick only with flush
     * methods.
     *
     * @param autoRemove flag whether to do auto-remove during every flush
     * @return this builder
     */
    public MemoryBufferBuilder<K, V> withAutoRemove(boolean autoRemove) {
        this.autoRemove = autoRemove;
        return this;
    }


    /**
     * Sets whether this buffer should invoke flush key on every {@link MemoryBuffer#put(Object, Bufferable[])}.
     * That means that {@link Bufferable#invokesFlush()} is ignored (acts like it is always true).
     *
     * @param flushOnEveryPut flag whether to do auto-remove during every flush
     * @return this builder
     */
    public MemoryBufferBuilder<K, V> withFlushOnEveryPut(boolean flushOnEveryPut) {
        this.flushOnEveryPut = flushOnEveryPut;
        return this;
    }

    /**
     * Builds an instance of the buffer.
     *
     * @return buffer instance
     */
    public MemoryBuffer<K, V> build() {
        return new MemoryBuffer<>(this);
    }

    KeyFlushStrategy<K, V> getFlushStrategy() {
        return flushStrategy;
    }

    LockMode getLockMode() {
        return lockMode;
    }

    String getName() {
        return name;
    }

    boolean isAutoRemove() {
        return autoRemove;
    }

    boolean isFlushOnEveryPut() {
        return flushOnEveryPut;
    }
}
