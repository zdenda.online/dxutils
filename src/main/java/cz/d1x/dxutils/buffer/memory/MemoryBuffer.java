package cz.d1x.dxutils.buffer.memory;

import cz.d1x.dxutils.buffer.*;
import cz.d1x.dxutils.lock.Lock;
import cz.d1x.dxutils.lock.LockedCollections;
import cz.d1x.dxutils.lock.LockedMap;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

/**
 * <p>
 * A buffer implementation that stores all values in the memory.
 * During flush, it calls given {@link KeyFlushStrategy} that is invoked separately for every key in the buffer.
 * Note that you should NOT call any operation on the buffer while you are within {@link KeyFlushStrategy}. That is
 * because that during flush you are in context of locks and you may cause dead-lock by calling any other method.
 * </p><p>
 * As this implementation stores everything in-memory, it is guaranteed that put methods always add values to the
 * buffer even if eventual flush fails.
 * This implementation supports all modes of {@link LockMode}.
 * </p><p>
 * Contract of {@link Buffer#flush(Object[])} is persisted as it does not releases all resources.
 * Anyway this buffer allows to set auto-removal which causes that during flush, key is also removed.
 * That practically means that clients does not need to call any of remove/clear methods and can call one of flush
 * methods instead.
 * </p>
 * <p>
 * To create a new instance, use {@link MemoryBufferBuilder} that also contains description of parameters of this buffer.
 * </p>
 *
 * @param <K> key type generic
 * @param <V> value type generic
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 * @see KeyFlushStrategy
 * @see MemoryBufferBuilder
 * @see Buffer
 * @see BufferAutoFlush
 */
public class MemoryBuffer<K, V extends Bufferable> implements Buffer<K, V> {

    private final LockedMap<K, List<V>> valuesMap = LockedCollections.newHashMap();

    private final Lock bufferLock = new Lock();
    private final LockedMap<K, Lock> keyLocks = LockedCollections.newHashMap();

    private final KeyFlushStrategy<K, V> flushStrategy;
    private final LockMode lockMode;
    private final boolean autoRemove;
    private final boolean flushOnEveryPut;
    private final String name;

    /**
     * Use builder for creation of new instances.
     *
     * @param builder builder for settings of buffer
     */
    protected MemoryBuffer(MemoryBufferBuilder<K, V> builder) {
        this.flushStrategy = builder.getFlushStrategy();
        this.lockMode = builder.getLockMode();
        this.autoRemove = builder.isAutoRemove();
        this.flushOnEveryPut = builder.isFlushOnEveryPut();
        this.name = builder.getName();
    }

    @Override
    public LockMode getLockMode() {
        return lockMode;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void put(K key, V... values) {
        put(key, Arrays.asList(values));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void put(K key, Collection<V> values) {
        inLock(key, () -> {
            List<V> currentValues = getOrCreateValues(key);
            currentValues.addAll(values);
            if (flushOnEveryPut || values.stream().anyMatch(Bufferable::invokesFlush)) {
                flush(key);
            }
        });
    }

    @Override
    public Set<K> keys() {
        return inLockWithReturn(() -> valuesMap.readWithReturn(map -> new HashSet<>(map.keySet())));
    }

    @Override
    public Map<K, Integer> size() {
        return inLockWithReturn(() -> {
            final Map<K, Integer> sizes = new HashMap<>();
            valuesMap.read(map -> map.keySet().forEach(key -> {
                List<V> values = map.get(key);
                sizes.put(key, values.size());
            }));
            return sizes;
        });
    }

    @Override
    public List<V> valuesOf(K key) {
        return inLockWithReturn(key, () ->
                valuesMap.readWithReturn(map -> {
                    List<V> values = map.get(key);
                    if (values == null) return null;
                    return new ArrayList<>(values);
                }));
    }

    @Override
    public boolean isClear() {
        return inLockWithReturn(() -> valuesMap.readWithReturn(Map::isEmpty));
    }

    @Override
    public boolean flush() {
        if (autoRemove) {
            return removeKeys(null);
        } else {
            return flushKeys(null);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean flush(K... keys) {
        if (autoRemove) {
            return removeKeys(Arrays.asList(keys));
        } else {
            return flushKeys(Arrays.asList(keys));
        }
    }

    @Override
    public boolean clear() {
        return removeKeys(null);
    }

    @Override
    public Map<K, List<V>> clearWithoutFlush() {
        return removeKeysWithoutFlush(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean remove(K... keys) {
        return removeKeys(Arrays.asList(keys));
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<K, List<V>> removeWithoutFlush(K... keys) {
        return removeKeysWithoutFlush(Arrays.asList(keys));
    }

    @Override
    public String toString() {
        return (name != null) ? name : super.toString();
    }

    private boolean flushKeys(Collection<K> keys) {
        AtomicBoolean allFlushesOk = new AtomicBoolean(true);
        Collection<K> keysToFlush = resolveKeys(keys);
        keysToFlush.forEach(key ->
                inLock(key, () -> {
                    List<V> values = valuesMap.readWithReturn(map -> map.get(key));
                    flushValues(key, values, allFlushesOk);
                }));
        return allFlushesOk.get();
    }

    private boolean removeKeys(Collection<K> keys) {
        AtomicBoolean allRemovesOk = new AtomicBoolean(true);
        Collection<K> keysToRemove = resolveKeys(keys);
        keysToRemove.forEach(key ->
                inLock(key, () -> {
                    List<V> values = valuesMap.readWithReturn(map -> map.get(key));
                    boolean flushed = flushValues(key, values, allRemovesOk);
                    if (flushed) {
                        valuesMap.write(map -> map.remove(key));
                        keyLocks.write(locks -> locks.remove(key));
                    }
                }));
        return allRemovesOk.get();
    }

    private Map<K, List<V>> removeKeysWithoutFlush(Collection<K> keys) {
        Map<K, List<V>> returnedValues = new HashMap<>();
        Collection<K> keysToRemove = resolveKeys(keys);
        keysToRemove.forEach(key ->
                inLock(key, () -> {
                    valuesMap.write(map -> {
                        List<V> values = map.remove(key);
                        if (values != null && !values.isEmpty()) returnedValues.put(key, new ArrayList<>(values));
                    });
                    keyLocks.write(locks -> locks.remove(key));
                }));
        return returnedValues;
    }

    private boolean flushValues(K key, List<V> values, AtomicBoolean allFlushesOk) {
        if (values == null || values.isEmpty()) return true;

        List<V> valuesCopy = new ArrayList<>(values);
        values.clear();

        boolean flushed = false;
        try {
            flushed = flushStrategy.flushKey(key, valuesCopy);
            return flushed;
        } finally {
            if (!flushed) {
                values.addAll(valuesCopy);
                allFlushesOk.set(false);
            }
        }
    }

    /**
     * If param is null, it means to remove all keys, otherwise return parameter.
     */
    private Collection<K> resolveKeys(Collection<K> keysParam) {
        if (keysParam == null) return valuesMap.readWithReturn(map -> new HashSet<>(map.keySet()));
        return keysParam;
    }

    private List<V> getOrCreateValues(K key) {
        List<V> currentValues = valuesMap.readWithReturn(map -> map.get(key));
        if (currentValues == null) {
            currentValues = valuesMap.writeWithReturn(map ->
                    map.computeIfAbsent(key, k -> new ArrayList<>()));
        }
        return currentValues;
    }

    private <R> R inLockWithReturn(Supplier<R> action) {
        return getLock(null).write(action);
    }

    private void inLock(K key, Runnable action) {
        Lock keyLock = getLock(key);
        if (lockMode == LockMode.LOCK_KEY) {
            try {
                if (hasCorrectLock(key, keyLock)) {
                    action.run();
                } else {
                    throw new InvalidLockAcquired();
                }
            } catch (InvalidLockAcquired e) {
                inLock(key, action); // try again
            }
        } else {
            keyLock.write(action);
        }
    }

    private <R> R inLockWithReturn(K key, Supplier<R> action) {
        Lock keyLock = getLock(key);
        if (lockMode == LockMode.LOCK_KEY) {
            try {
                return keyLock.write(() -> {
                    if (hasCorrectLock(key, keyLock)) {
                        return action.get();
                    } else {
                        throw new InvalidLockAcquired();
                    }
                });
            } catch (InvalidLockAcquired e) {
                return inLockWithReturn(key, action); // try again
            }
        } else {
            return keyLock.write(action);
        }
    }

    private Lock getLock(K key) {
        switch (lockMode) {
            case LOCK_BUFFER:
                return bufferLock;
            case LOCK_KEY: {
                if (key == null) return bufferLock; // this is for special cases like counting size and so on
                Lock keyLock = keyLocks.readWithReturn(map -> map.get(key));
                if (keyLock == null) {
                    keyLock = keyLocks.writeWithReturn(map -> map.computeIfAbsent(key, k -> new Lock()));
                }
                return keyLock;
            }
            case NO_LOCK:
                return new Lock(); // one-time lock that will never block anyone
            default:
                throw new IllegalStateException("Unsupported lock mode " + lockMode);
        }
    }

    private boolean hasCorrectLock(K key, Lock acquiredLock) {
        return keyLocks.readWithReturn(map -> map.get(key) == acquiredLock);
    }

    private static class InvalidLockAcquired extends RuntimeException {
    }
}
