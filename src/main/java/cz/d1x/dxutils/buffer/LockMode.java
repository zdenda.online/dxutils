package cz.d1x.dxutils.buffer;

/**
 * <p>
 * Mode of thread safety of {@link Buffer}.
 * Every buffer implementation supports some (or all) modes of lock:
 * </p>
 * <ul>
 * <li>{@link #LOCK_BUFFER} - The safest but also slowest mode.
 * Buffer gets locked and blocks concurrent operations of ANY key.
 * Typically you want to use this mode if underlying storage does not guarantee any thread safety.</li>
 * <li>{@link #LOCK_KEY} - Usually a lot faster than {@link #LOCK_BUFFER} but slower than {@link #NO_LOCK}.
 * Buffer gets locked and blocks concurrent operations of the SAME key.
 * Typically you want to use this mode if underlying storage guarantees thread-safety per key but not across various keys.</li>
 * <li>{@link #NO_LOCK} - The fastest but less safe mode.
 * Buffer does not block and allows any concurrent operations leaving client to synchronize access to it.
 * </ul>
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 * @see Buffer
 */
public enum LockMode {

    /**
     * The safest but also slowest mode.
     * Buffer gets locked and blocks concurrent operations of ANY key.
     * Typically you want to use this mode if underlying storage does not guarantee any thread safety.
     */
    LOCK_BUFFER,

    /**
     * Usually a lot faster than {@link #LOCK_BUFFER} but slower than {@link #NO_LOCK}.
     * Buffer gets locked and blocks concurrent operations of the SAME key.
     * Typically you want to use this mode if underlying storage guarantees thread-safety per key but not across various keys.
     */
    LOCK_KEY,

    /**
     * The fastest but less safe mode.
     * Buffer does not block and allows any concurrent operations leaving client to synchronize access to it.
     */
    NO_LOCK
}
