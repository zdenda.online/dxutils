package cz.d1x.dxutils.lock;

import java.util.Map;

/**
 * Predefined locked map for easier use in the code.
 * See {@link Locked} for more information.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 * @see Locked
 */
public class LockedMap<K, V> extends Locked<Map<K, V>> {

    /**
     * Creates a new locked map with given implementation.
     *
     * @param value map implementation to be used
     */
    public LockedMap(Map<K, V> value) {
        super(value);
    }

    /**
     * Creates a new locked map with given implementation.
     *
     * @param lock  lock to be used
     * @param value map implementation to be used
     */
    public LockedMap(Lock lock, Map<K, V> value) {
        super(lock, value);
    }
}
