package cz.d1x.dxutils.lock;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Allows to lock given generic value with {@link Lock} to allow more elegant lambda-style of work with the value
 * under the lock.
 *
 * @param <V> generic of value to be tied with the lock
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 * @see Lock
 */
public class Locked<V> {

    private final Lock lock;
    private final V value;

    /**
     * Creates a new locked value with implicit fair lock.
     *
     * @param value value to be locked
     * @throws IllegalArgumentException possible exception if value is null
     */
    public Locked(V value) {
        this(new Lock(true), value);
    }

    /**
     * Creates a new locked value with given lock implementation.
     *
     * @param lock  lock to be used
     * @param value value to be locked
     * @throws IllegalArgumentException possible exception if lock or value is null
     */
    public Locked(Lock lock, V value) {
        if (lock == null) throw new IllegalArgumentException("Lock cannot be null");
        this.lock = lock;

        if (value == null) throw new IllegalArgumentException("Value cannot be null");
        this.value = value;
    }

    /**
     * Reads current value (within read lock).
     * Note that if your value is not immutable, you should NOT be using this method but rather
     * {@link #readWithReturn(Function)} and create a copy within its lambda.
     * Otherwise any further mutable change of value will be out of scope lock and may break consistency.
     *
     * @return values
     */
    public V readValue() {
        return lock.read(() -> this.value);
    }

    /**
     * Performs a given action on value with no return value in read lock.
     * Note that multiple read operations does not block themselves.
     *
     * @param action action to be performed
     */
    public void read(Consumer<V> action) {
        lock.read(() -> action.accept(this.value));
    }

    /**
     * Performs a given action on value with no return value in write lock.
     * Note that write lock is exclusive (so other threads attempting to read or write will be blocked until
     * given action finishes and releases the lock).
     *
     * @param action action to be performed
     */
    public void write(Consumer<V> action) {
        lock.write(() -> action.accept(this.value));
    }

    /**
     * Performs a given action on value with return value in read lock.
     * Note that multiple read operations does not block themselves.
     *
     * @param action action to be performed
     * @param <R>    generic type of return value of action
     * @return return value of action
     */
    public <R> R readWithReturn(Function<V, R> action) {
        return lock.read(() -> action.apply(this.value));
    }

    /**
     * Performs a given action on value with return value in write lock.
     * Note that write lock is exclusive (so other threads attempting to read or write will be blocked until
     * given action finishes and releases the lock).
     *
     * @param action action to be performed
     * @param <R>    generic type of return value of action
     * @return return value of action
     */
    public <R> R writeWithReturn(Function<V, R> action) {
        return lock.write(() -> action.apply(this.value));
    }

}
