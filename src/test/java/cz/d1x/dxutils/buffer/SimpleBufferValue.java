package cz.d1x.dxutils.buffer;

/**
 * Simple buffer value for testing.
 */
public class SimpleBufferValue implements Bufferable {

    private final Integer id;
    private final Integer key;
    private final boolean invokesFlush;

    public SimpleBufferValue(Integer id, Integer key, boolean invokesFlush) {
        this.id = id;
        this.key = key;
        this.invokesFlush = invokesFlush;
    }

    @Override
    public boolean invokesFlush() {
        return invokesFlush;
    }

    public Integer getId() {
        return id;
    }

    public Integer getKey() {
        return key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimpleBufferValue value = (SimpleBufferValue) o;
        return id.equals(value.id) && key.equals(value.key);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + key.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SimpleBufferValue{" +
                "id=" + id +
                ", key=" + key +
                ", invokesFlush=" + invokesFlush +
                '}';
    }
}
