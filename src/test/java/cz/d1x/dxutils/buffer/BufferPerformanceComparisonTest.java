package cz.d1x.dxutils.buffer;

import cz.d1x.dxutils.util.RepeatTest;
import cz.d1x.dxutils.util.RepeatTestRule;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import java.util.Date;
import java.util.function.Function;

@Ignore("For manual performance testing only")
public class BufferPerformanceComparisonTest {

    @Rule
    public RepeatTestRule repeatTestRule = new RepeatTestRule();

    @Test
    @RepeatTest(3) // can repeat multiple times if needed (that is better warm-up)
    public void comparePerformance() {
//        printHistoricalMeasurements();

        // can use different private methods or construct custom parameters
//        ComplexBufferTest.ComplexBufferTestParams params = highLoadAndConcurrencyParams();
        ComplexBufferTest.ComplexBufferTestParams params = realUseCaseParams();
//        ComplexBufferTest.ComplexBufferTestParams params = clearThroughputParams();

//        params.printInfo = true; // if it hangs up for too long...

        System.out.println("Starting buffers performance tests (" + new Date() + ")\n" +
                "Parameters:\n\t" + params.toString().replace("\n", "\n\t"));

        doTest("MemoryBuffer (flush buffer lock)", params, strategy -> Buffers.memory(strategy)
                .withLockMode(LockMode.LOCK_BUFFER)
                .build());
        doTest("MemoryBuffer (value lock)", params, strategy -> Buffers.memory(strategy)
                .withLockMode(LockMode.LOCK_KEY)
                .build());
        doTest("MemoryBuffer (value lock + auto-remove)", params, strategy -> Buffers.memory(strategy)
                .withLockMode(LockMode.LOCK_KEY)
                .withAutoRemove(true)
                .build());
        doTest("MemoryBuffer (flush no lock)", params, strategy -> Buffers.memory(strategy)
                .withLockMode(LockMode.NO_LOCK)
                .build());
        doTest("SingleMutexBuffer", params, SingleMutexMemoryBuffer::new);

    }

    private long doTest(String name, ComplexBufferTest.ComplexBufferTestParams params,
                        Function<KeyFlushStrategy<Integer, ComplexBufferTest.TestValue>, Buffer<Integer, ComplexBufferTest.TestValue>> bufferFactory) {
        System.out.println("Testing " + name);
        ComplexBufferTest test = new ComplexBufferTest(params, bufferFactory);
        long start = System.currentTimeMillis();
        test.checkBuffer();
        long elapsed = System.currentTimeMillis() - start;
        System.out.println("\tfinished in " + elapsed + "ms");
        return elapsed;
    }

    private ComplexBufferTest.ComplexBufferTestParams realUseCaseParams() {
        ComplexBufferTest.ComplexBufferTestParams params = new ComplexBufferTest.ComplexBufferTestParams();
        params.putSleep = 1;
        params.flushSleep = 3; // to differentiate flush with and without lock
        params.autoFlushInterval = 100;
        params.putThreads = 150;

        params.keysCount = 300;
        params.putsPerKey = 5;
        params.valuesPerPut = 2;

        params.percentsOfFlushValues = 10;
        params.simulateFlushFailures = false; // no flush failures
        params.testTimeoutSeconds = 60; // usually around 10s, 1min must be enough
        return params;
    }

    private ComplexBufferTest.ComplexBufferTestParams highLoadAndConcurrencyParams() {
        ComplexBufferTest.ComplexBufferTestParams params = new ComplexBufferTest.ComplexBufferTestParams();
        params.putSleep = 1;
        params.flushSleep = 3;
        params.autoFlushInterval = 50;
        params.putThreads = 500;

        params.keysCount = 1000;
        params.putsPerKey = 500;
        params.valuesPerPut = 1;

        params.percentsOfFlushValues = 15; // higher flush
        params.simulateFlushFailures = true; // no flush failures
        params.percentsOfFlushFailures = 10; // some failures to make more repeated flushes
        params.testTimeoutSeconds = 10 * 60; // usually around 5min, 10min must be enough
        return params;
    }

    private ComplexBufferTest.ComplexBufferTestParams clearThroughputParams() {
        ComplexBufferTest.ComplexBufferTestParams params = new ComplexBufferTest.ComplexBufferTestParams();
        params.putSleep = 0;
        params.flushSleep = 0;
        params.autoFlushInterval = 20; // it is very fast so catch up few times
        params.putThreads = 500;

        params.keysCount = 500;
        params.putsPerKey = 10;
        params.valuesPerPut = 1;

        params.percentsOfFlushValues = 20;
        params.simulateFlushFailures = false; // no flush failures
        params.testTimeoutSeconds = 10; // usually under 1, 10s must be enough
        return params;
    }

    private void printHistoricalMeasurements() {
        System.out.println("======================================================");
        System.out.println("Historical run on i5 notebook:\n" +
                "Parameters:\n" +
                "\tputThreads=1000\n" +
                "\tautoFlushInterval=30\n" +
                "\tautoFlushConstantDelay=false\n" +
                "\tputSleep=2\n" +
                "\tflushSleep=10\n" +
                "\tkeysCount=1000\n" +
                "\tputsPerKey=10\n" +
                "\tvaluesPerPut=2\n" +
                "\tvaluesPerKey=20\n" +
                "\tpercentsOfFlushValues=20\n" +
                "\tsimulateFlushFailures=true\n" +
                "\tpercentsOfFlushFailures=20\n" +
                "Testing MemoryBuffer (flush buffer lock)\n" +
                "\tfinished in 54330ms\n" +
                "Testing MemoryBuffer (value lock)\n" +
                "\tfinished in 13404ms\n" +
                "Testing MemoryBuffer (flush no lock)\n" +
                "\tfinished in 10269ms\n" +
                "Testing SingleMutexBuffer\n" +
                "\tfinished in 67510ms");
        System.out.println("======================================================");
        System.out.println("Historical run on i7 desktop:\n" +
                "Starting buffers performance tests (Wed Feb 15 23:37:24 CET 2017)\n" +
                "Parameters:\n" +
                "\tputThreads=500\n" +
                "\tautoFlushInterval=50\n" +
                "\tautoFlushConstantDelay=false\n" +
                "\tputSleep=1\n" +
                "\tflushSleep=3\n" +
                "\tkeysCount=1000\n" +
                "\tputsPerKey=500\n" +
                "\tvaluesPerPut=1\n" +
                "\tpercentsOfFlushValues=15\n" +
                "\tsimulateFlushFailures=true\n" +
                "\tpercentsOfFlushFailures=10\n" +
                "Testing MemoryBuffer (flush buffer lock)\n" +
                "\tfinished in 328272ms\n" +
                "Testing MemoryBuffer (value lock)\n" +
                "\tfinished in 14198ms\n" +
                "Testing MemoryBuffer (value lock + auto-remove)\n" +
                "\tfinished in 16241ms\n" +
                "Testing MemoryBuffer (flush no lock)\n" +
                "\tfinished in 12957ms\n" +
                "Testing SingleMutexBuffer\n" +
                "\tfinished in 374020ms");
        System.out.println("======================================================");
    }
}
