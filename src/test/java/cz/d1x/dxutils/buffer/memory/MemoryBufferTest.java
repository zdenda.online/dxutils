package cz.d1x.dxutils.buffer.memory;

import cz.d1x.dxutils.buffer.*;
import cz.d1x.dxutils.util.RepeatTest;
import cz.d1x.dxutils.util.RepeatTestRule;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.*;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Tests {@link MemoryBuffer}
 */
public class MemoryBufferTest {

    @Rule
    public RepeatTestRule repeatTestRule = new RepeatTestRule();
    @Mock
    private KeyFlushStrategy<Integer, SimpleBufferValue> flushStrategy;
    @Captor
    private ArgumentCaptor<List<SimpleBufferValue>> flushedValues;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void bufferNameIsOutputOfToString() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer("X marks the spot");

        Assert.assertEquals("X marks the spot", buffer.toString());
    }

    @Test
    public void bufferWithNullNameKeepsOriginalToString() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer((String) null);

        Assert.assertTrue(buffer.toString().startsWith(MemoryBuffer.class.getName() + "@"));
    }

    @Test
    public void bufferSizeIsCorrectAfterPuts() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer();
        mockAllFlushesReturnTrue();

        putSimpleValue(buffer, 1, 1, false);
        putSimpleValue(buffer, 2, 2, false);
        verifyBufferSizes(buffer, 1, 1);

        putSimpleValue(buffer, 3, 1, false);
        putSimpleValue(buffer, 4, 3, false);
        verifyBufferSizes(buffer, 2, 1, 1);
    }

    @Test
    public void bufferSizeIsCorrectAfterFlush() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer();
        mockAllFlushesReturnTrue();

        putSimpleValue(buffer, 1, 1, false);
        putSimpleValue(buffer, 2, 2, false);
        buffer.flush();

        verifyBufferSizes(buffer, 0, 0);
    }

    @Test
    public void isClearUponCreation() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer();

        Assert.assertTrue(buffer.isClear());
    }

    @Test
    public void isNotClearAfterFlush() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer();

        putSimpleValue(buffer, 1, 1, false);
        buffer.flush();

        Assert.assertFalse(buffer.isClear());
    }

    @Test
    public void isClearAfterSinglePutAndRemove() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer();
        mockAllFlushesReturnTrue();

        putSimpleValue(buffer, 1, 1, false);
        buffer.remove(1);

        Assert.assertTrue(buffer.isClear());
    }

    @Test
    public void noKeysUponCreation() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer();

        verifyHasKeys(buffer);
    }

    @Test
    public void correctKeysAfterPut() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer();

        putSimpleValue(buffer, 1, 1, false);
        putSimpleValue(buffer, 2, 2, false);
        putSimpleValue(buffer, 3, 2, false);

        verifyHasKeys(buffer, 1, 2);
    }

    @Test
    public void valuesOfReturnsCorrectValuesOfKey() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer();

        SimpleBufferValue value1 = putSimpleValue(buffer, 1, 1, false);
        SimpleBufferValue value2 = putSimpleValue(buffer, 2, 2, false);
        SimpleBufferValue value3 = putSimpleValue(buffer, 3, 2, false);

        verifyHasValues(buffer, 1, value1);
        verifyHasValues(buffer, 2, value2, value3);
    }

    @Test
    public void valuesOfReturnsNullIfKeyNotInBuffer() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer();

        putSimpleValue(buffer, 1, 1, false);
        putSimpleValue(buffer, 2, 2, false);
        putSimpleValue(buffer, 3, 2, false);

        Assert.assertNull(buffer.valuesOf(0));
        Assert.assertNull(buffer.valuesOf(4));
    }

    @Test
    public void autoRemoveDuringPutOfFlushingValuesCausesBufferToBeClearAfterwards() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer(true);
        mockAllFlushesReturnTrue();

        putSimpleValue(buffer, 1, 1, true);

        Assert.assertTrue(buffer.isClear());
    }


    @Test
    public void autoRemoveAfterExplicitFlushCausesBufferToBeClearAfterwards() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer(true);
        mockAllFlushesReturnTrue();

        putSimpleValue(buffer, 1, 1, false);
        buffer.flush(1);

        Assert.assertTrue(buffer.isClear());
    }

    @Test
    public void flushOnEveryPutInvokesFlushEvenWhenValueDoesNotInvokeFlush() {
        Buffer<Integer, SimpleBufferValue> buffer = Buffers.memory(flushStrategy)
                .withFlushOnEveryPut(true)
                .build();
        mockAllFlushesReturnTrue();

        SimpleBufferValue value = putSimpleValue(buffer, 1, 1, false);

        verifyAndCaptureFlush(1, 1);
        verifyFlushedValue(1, value);
    }

    @Test
    public void removesAllValuesWithoutFlush() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer();

        SimpleBufferValue value1 = putSimpleValue(buffer, 1, 1, false);
        SimpleBufferValue value2 = putSimpleValue(buffer, 2, 1, false);
        SimpleBufferValue value3 = putSimpleValue(buffer, 3, 2, false);
        Map<Integer, List<SimpleBufferValue>> removedValues = buffer.clearWithoutFlush();

        Assert.assertEquals(2, removedValues.size());
        verifyRemovedValue(removedValues, 1, value1);
        verifyRemovedValue(removedValues, 1, value2);
        verifyRemovedValue(removedValues, 2, value3);
        Assert.assertTrue(buffer.isClear());
        Mockito.verify(flushStrategy, Mockito.never()).flushKey(Mockito.any(), Mockito.any());
    }

    @Test
    public void removesAllValuesWithFlush() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer();
        mockAllFlushesReturnTrue();

        SimpleBufferValue value1 = putSimpleValue(buffer, 1, 1, false);
        SimpleBufferValue value2 = putSimpleValue(buffer, 2, 1, false);
        SimpleBufferValue value3 = putSimpleValue(buffer, 3, 2, false);
        boolean cleared = buffer.clear();

        Assert.assertTrue(cleared);
        Assert.assertTrue(buffer.isClear());
        verifyAndCaptureFlush(1, 1);
        verifyAndCaptureFlush(2, 1);
        verifyFlushedValue(1, value1, value2);
        verifyFlushedValue(2, value3);
    }

    @Test
    public void removesSpecificValuesWithFlush() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer();
        mockAllFlushesReturnTrue();

        putSimpleValue(buffer, 1, 1, false);
        putSimpleValue(buffer, 2, 1, false);
        SimpleBufferValue value3 = putSimpleValue(buffer, 3, 2, false);
        boolean cleared = buffer.remove(2);

        Assert.assertTrue(cleared);
        Assert.assertFalse(buffer.isClear()); // there are still values of key 1
        verifyAndCaptureFlush(1, 0);
        verifyAndCaptureFlush(2, 1);
        verifyFlushedValue(1, value3);
    }

    @Test
    public void removeWithoutFlushOnEmptyBufferDoesNotReturnAnyValue() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer();

        Map<Integer, List<SimpleBufferValue>> removed = buffer.removeWithoutFlush(1);

        Assert.assertTrue(removed.isEmpty());
    }

    @Test
    public void removesSpecificValuesWithoutFlush() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer();
        mockAllFlushesReturnTrue();

        putSimpleValue(buffer, 1, 1, false);
        putSimpleValue(buffer, 2, 1, false);
        SimpleBufferValue value3 = putSimpleValue(buffer, 3, 2, false);
        SimpleBufferValue value4 = putSimpleValue(buffer, 4, 2, false);
        Map<Integer, List<SimpleBufferValue>> removed = buffer.removeWithoutFlush(2);

        Assert.assertNotNull(removed);
        Assert.assertFalse(removed.isEmpty());
        Assert.assertFalse(buffer.isClear()); // there are still values of key 1
        verifyNoFlushes(2);
        verifyRemovedValue(removed, 2, value3);
        verifyRemovedValue(removed, 2, value4);
    }

    @Test
    public void removeOnEmptyBufferDoesNotReturnsTrue() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer();

        boolean removed = buffer.remove(1);

        Assert.assertTrue(removed);
    }

    @Test
    public void unsuccessfulFlushDuringPutKeepsDataInBuffer() {
        FailingFlushStrategy flushStrategy = new FailingFlushStrategy(3); // every 3rd flush fails

        Buffer<Integer, SimpleBufferValue> buffer = newBuffer(flushStrategy);

        putSimpleValue(buffer, 1, false);
        putSimpleValue(buffer, 2, true); // invokes first flush that succeeds
        flushStrategy.verifyFlushedValues(1, 2);

        putSimpleValue(buffer, 3, true); // invokes second flush that succeeds
        flushStrategy.verifyFlushedValues(1, 2, 3);

        putSimpleValue(buffer, 4, false);
        putSimpleValue(buffer, 5, true); // invokes third flush that fails
        flushStrategy.verifyFlushedValues(1, 2, 3); // nothing was flushed this time

        putSimpleValue(buffer, 6, true); // invokes fourth flush that fails
        flushStrategy.verifyFlushedValues(1, 2, 3, 4, 5, 6); // also with values that previously failed
    }

    @Test
    public void unsuccessfulFlushDuringRemoveKeepsDataInBuffer() {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer(flushStrategy);
        Mockito.when(flushStrategy.flushKey(Mockito.any(), Mockito.any())).thenReturn(false);
        int key = 1;

        SimpleBufferValue value = putSimpleValue(buffer, key, key, false);
        buffer.remove(key);

        verifyHasValues(buffer, key, value);
    }

    @Test
    public void complexTestWithFlushLocksBuffer() {
        ComplexBufferTest.ComplexBufferTestParams params = new ComplexBufferTest.ComplexBufferTestParams();
        params.flushSleep = 0;
        params.putSleep = 2;
        params.testTimeoutSeconds = 60; // 1 minute must be enough, on regular machines it takes about 5-10 seconds

        ComplexBufferTest test = new ComplexBufferTest(params, strategy -> Buffers.memory(strategy)
                .withLockMode(LockMode.LOCK_BUFFER)
                .build());
        test.checkBuffer();
    }

    @Test
    public void complexTestWithFlushLocksValue() {
        ComplexBufferTest.ComplexBufferTestParams params = new ComplexBufferTest.ComplexBufferTestParams();
        params.flushSleep = 2;
        params.putSleep = 1;
        params.testTimeoutSeconds = 60; // 1 minute must be enough, on regular machines it takes about 5-10 seconds

        ComplexBufferTest test = new ComplexBufferTest(params, strategy -> Buffers.memory(strategy)
                .withLockMode(LockMode.LOCK_KEY)
                .build());
        test.checkBuffer();
    }

    @Test
    public void complexTestWithoutFlushLock() {
        ComplexBufferTest.ComplexBufferTestParams params = new ComplexBufferTest.ComplexBufferTestParams();
        params.flushSleep = 2;
        params.putSleep = 1;
        params.testTimeoutSeconds = 60; // 1 minute must be enough, on regular machines it takes about 5-10 seconds

        ComplexBufferTest test = new ComplexBufferTest(params, strategy -> Buffers.memory(strategy)
                .withLockMode(LockMode.NO_LOCK)
                .build());
        test.checkBuffer();
    }

    @Test
    @RepeatTest(100)
    public void concurrentRemoveWithoutFlushAndFlushDoesNotDuplicateOrLoseValueWithLockBufferMode() {
        concurrentRemoveWithoutFlushAndFlushDoesNotDuplicateOrLoseValueWithMode(LockMode.LOCK_BUFFER);
    }

    @Test
    @RepeatTest(100)
    public void concurrentRemoveWithoutFlushAndFlushDoesNotDuplicateOrLoseValueWithLockKeyMode() {
        concurrentRemoveWithoutFlushAndFlushDoesNotDuplicateOrLoseValueWithMode(LockMode.LOCK_KEY);
    }

    @Test
    @RepeatTest(100)
    public void concurrentRemoveWithoutFlushAndFlushDoesNotDuplicateOrLoseValueWithNoLockMode() {
        concurrentRemoveWithoutFlushAndFlushDoesNotDuplicateOrLoseValueWithMode(LockMode.NO_LOCK);
    }

    private void concurrentRemoveWithoutFlushAndFlushDoesNotDuplicateOrLoseValueWithMode(LockMode mode) {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer(mode);
        mockAllFlushesReturnTrue();
        Integer key = 0;

        final SimpleBufferValue value = putSimpleValue(buffer, key, key, false);
        AtomicBoolean removed = new AtomicBoolean(false);

        doTwoConcurrentActions(
                buffer::flush,
                () -> {
                    Map<Integer, List<SimpleBufferValue>> removedValues = buffer.removeWithoutFlush(key);
                    if (removedValues.get(key) != null && removedValues.get(key).contains(value)) {
                        removed.set(true);
                    }
                });

        if (removed.get()) {
            verifyNoFlushes(key);
        } else {
            verifyAndCaptureFlush(key, 1);
            verifyFlushedValue(1, value);
        }
    }

    @Test
    @RepeatTest(100)
    public void concurrentPutAndRemoveWithoutFlushDoesNotDuplicateOrLoseValueWithLockBufferMode() {
        concurrentPutAndRemoveWithoutFlushDoesNotDuplicateOrLoseValueWithMode(LockMode.LOCK_BUFFER);
    }

    @Test
    @RepeatTest(100)
    public void concurrentPutAndRemoveWithoutFlushDoesNotDuplicateOrLoseValueWithLockKeyMode() {
        concurrentPutAndRemoveWithoutFlushDoesNotDuplicateOrLoseValueWithMode(LockMode.LOCK_KEY);
    }

    @Test
    @RepeatTest(100)
    public void concurrentPutAndRemoveWithoutFlushDoesNotDuplicateOrLoseValueWithNoLockMode() {
        concurrentPutAndRemoveWithoutFlushDoesNotDuplicateOrLoseValueWithMode(LockMode.NO_LOCK);
    }

    private void concurrentPutAndRemoveWithoutFlushDoesNotDuplicateOrLoseValueWithMode(LockMode mode) {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer(mode);
        mockAllFlushesReturnTrue();
        final Integer key = 0;

        final SimpleBufferValue value = new SimpleBufferValue(key, key, false);
        AtomicBoolean removed = new AtomicBoolean(false);

        doTwoConcurrentActions(
                () -> buffer.put(key, value),
                () -> {
                    Map<Integer, List<SimpleBufferValue>> removedValues = buffer.removeWithoutFlush(key);
                    if (removedValues.get(key) != null && removedValues.get(key).contains(value)) {
                        removed.set(true);
                    }
                });

        if (removed.get()) {
            verifyHasNotValues(buffer, key, value);
        } else {
            verifyHasValues(buffer, key, value);
        }
    }


    @Test
    @RepeatTest(100)
    public void concurrentPutAndFlushDoesNotDuplicateOrLoseValueWithLockBufferMode() {
        concurrentPutAndFlushDoesNotDuplicateOrLoseValueWithMode(LockMode.LOCK_BUFFER);
    }


    @Test
    @RepeatTest(100)
    public void concurrentPutAndFlushDoesNotDuplicateOrLoseValueWithLockKeyMode() {
        concurrentPutAndFlushDoesNotDuplicateOrLoseValueWithMode(LockMode.LOCK_KEY);
    }

    @Test
    @RepeatTest(100)
    public void concurrentPutAndFlushDoesNotDuplicateOrLoseValueWithNoLockMode() {
        concurrentPutAndFlushDoesNotDuplicateOrLoseValueWithMode(LockMode.NO_LOCK);
    }

    private void concurrentPutAndFlushDoesNotDuplicateOrLoseValueWithMode(LockMode mode) {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer(mode);
        mockAllFlushesReturnTrue();
        final Integer key = 0;

        final SimpleBufferValue value = new SimpleBufferValue(key, key, false);

        doTwoConcurrentActions(
                () -> buffer.put(key, value),
                () -> buffer.flush(key));

        List<SimpleBufferValue> bufferValues = buffer.valuesOf(key);
        if (bufferValues.isEmpty()) {
            verifyAndCaptureFlush(key, 1);
            verifyFlushedValue(1, value);
        } else {
            verifyHasValues(buffer, key, value);
        }
    }

    @Test
    @RepeatTest(100)
    public void concurrentFlushAndFlushDoesNotDuplicateOrLoseValueWithLockBufferMode() {
        concurrentFlushAndFlushDoesNotDuplicateOrLoseValueWithMode(LockMode.LOCK_BUFFER);
    }


    @Test
    @RepeatTest(100)
    public void concurrentFlushAndFlushDoesNotDuplicateOrLoseValueWithLockKeyMode() {
        concurrentFlushAndFlushDoesNotDuplicateOrLoseValueWithMode(LockMode.LOCK_KEY);
    }

    private void concurrentFlushAndFlushDoesNotDuplicateOrLoseValueWithMode(LockMode mode) {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer(mode);
        mockAllFlushesReturnTrue();
        final Integer key = 0;

        final SimpleBufferValue value = new SimpleBufferValue(key, key, false);
        buffer.put(key, value);

        doTwoConcurrentActions(
                () -> buffer.flush(key),
                () -> buffer.flush(key));

        verifyAndCaptureFlush(key, 1); // only one flush!!!
        verifyFlushedValue(1, value);
    }

    @Test
    @RepeatTest(100)
    public void concurrentFailedFlushAndRemoveWithFlushDoesNotLoseValueWithLockBufferMode() {
        concurrentFailedFlushAndRemoveWithFlushDoesNotLoseValueWithMode(LockMode.LOCK_BUFFER);
    }

    @Test
    @RepeatTest(100)
    public void concurrentFailedFlushAndRemoveWithFlushDoesNotLoseValueWithLockKeyMode() {
        concurrentFailedFlushAndRemoveWithFlushDoesNotLoseValueWithMode(LockMode.LOCK_KEY);
    }

    private void concurrentFailedFlushAndRemoveWithFlushDoesNotLoseValueWithMode(LockMode mode) {
        Buffer<Integer, SimpleBufferValue> buffer = newBuffer(mode);
        Mockito.when(flushStrategy.flushKey(Mockito.any(), Mockito.any())).thenReturn(false);
        final Integer key = 0;

        final SimpleBufferValue removedValue = new SimpleBufferValue(1, key, false);
        buffer.put(key, removedValue);

        final SimpleBufferValue newPutValue = new SimpleBufferValue(2, key, true);

        doTwoConcurrentActions(
                () -> buffer.remove(key),
                () -> buffer.put(key, newPutValue));

        verifyHasValues(buffer, key, removedValue, newPutValue);
    }

    /**
     * Puts simple value to the buffer, all values put this way will have the same key.
     */
    private SimpleBufferValue putSimpleValue(Buffer<Integer, SimpleBufferValue> buffer, Integer id, boolean invokesFlush) {
        return putSimpleValue(buffer, id, 27, invokesFlush);
    }

    private SimpleBufferValue putSimpleValue(Buffer<Integer, SimpleBufferValue> buffer, Integer id, Integer key, boolean invokesFlush) {
        SimpleBufferValue value = new SimpleBufferValue(id, key, invokesFlush);
        buffer.put(value.getKey(), value);
        return value;
    }

    private void verifyBufferSizes(Buffer<Integer, SimpleBufferValue> buffer, int... sizes) {
        Map<Integer, Integer> bufferSize = buffer.size();
        for (int idx = 0; idx < sizes.length; idx++) {
            int expectedSize = sizes[idx];
            Assert.assertEquals("Invalid size", expectedSize, bufferSize.get(idx + 1).intValue());
        }
    }

    private void verifyRemovedValue(Map<Integer, List<SimpleBufferValue>> removedValues, Integer key, SimpleBufferValue value) {
        Assert.assertTrue(removedValues.get(key).contains(value));
    }

    private void mockAllFlushesReturnTrue() {
        Mockito.when(flushStrategy.flushKey(Mockito.any(), Mockito.any())).thenReturn(true);
    }

    private void verifyAndCaptureFlush(Integer key, int invocationsCount) {
        Mockito.verify(flushStrategy, Mockito.times(invocationsCount)).flushKey(Mockito.eq(key), flushedValues.capture());
    }

    private void verifyNoFlushes(Integer key) {
        verifyAndCaptureFlush(key, 0);
    }

    private void verifyFlushedValue(int flushCall, SimpleBufferValue... values) {
        List<SimpleBufferValue> capturedValues = flushedValues.getAllValues().get(flushCall - 1);
        Arrays.stream(values).forEach(v -> Assert.assertTrue(capturedValues.contains(v)));
    }

    private void verifyHasKeys(Buffer<Integer, SimpleBufferValue> buffer, Integer... keys) {
        Set<Integer> actualKeys = buffer.keys();
        Assert.assertEquals("Expected and actual count of keys does not equal", keys.length, actualKeys.size());
        Arrays.stream(keys).forEach(key -> Assert.assertTrue("Expected to have key " + key + " in buffer but not present", actualKeys.contains(key)));
    }

    private void verifyHasValues(Buffer<Integer, SimpleBufferValue> buffer, Integer key, SimpleBufferValue... values) {
        List<SimpleBufferValue> bufferValues = buffer.valuesOf(key);
        Assert.assertNotNull("Expected non-null values of key " + key, bufferValues);
        Arrays.stream(values).forEach(v -> Assert.assertTrue("Expected to have value " + v + " in buffer but not present", bufferValues.contains(v)));
    }

    private void verifyHasNotValues(Buffer<Integer, SimpleBufferValue> buffer, Integer key, SimpleBufferValue... values) {
        List<SimpleBufferValue> bufferValues = buffer.valuesOf(key);
        if (bufferValues == null) return; // that is ok
        Arrays.stream(values).forEach(v -> Assert.assertFalse(bufferValues.contains(v)));
    }

    private MemoryBuffer<Integer, SimpleBufferValue> newBuffer(KeyFlushStrategy<Integer, SimpleBufferValue> flushStrategy,
                                                               String name,
                                                               LockMode lockMode,
                                                               boolean autoRemove) {
        return Buffers.memory(flushStrategy)
                .withName(name)
                .withLockMode(lockMode)
                .withAutoRemove(autoRemove)
                .build();
    }

    private MemoryBuffer<Integer, SimpleBufferValue> newBuffer(KeyFlushStrategy<Integer, SimpleBufferValue> flushStrategy) {
        return newBuffer(flushStrategy, null, LockMode.LOCK_KEY, false);
    }

    private MemoryBuffer<Integer, SimpleBufferValue> newBuffer(String name) {
        return newBuffer(flushStrategy, name, LockMode.LOCK_KEY, false);
    }

    private MemoryBuffer<Integer, SimpleBufferValue> newBuffer(LockMode lockMode) {
        return newBuffer(flushStrategy, null, lockMode, false);
    }

    private MemoryBuffer<Integer, SimpleBufferValue> newBuffer(boolean autoRemove) {
        return newBuffer(flushStrategy, null, LockMode.LOCK_KEY, autoRemove);
    }


    private MemoryBuffer<Integer, SimpleBufferValue> newBuffer() {
        return newBuffer(flushStrategy, null, LockMode.LOCK_KEY, false);
    }


    private void doTwoConcurrentActions(Runnable action1, Runnable action2) {
        ScheduledExecutorService threadPool = Executors.newScheduledThreadPool(2);
        CountDownLatch bothOperationsLatch = new CountDownLatch(2);

        threadPool.schedule(() -> {
            action1.run();
            bothOperationsLatch.countDown();
        }, 3, TimeUnit.MILLISECONDS); // schedule both to be executed in the same time
        threadPool.schedule(() -> {
            action2.run();
            bothOperationsLatch.countDown();
        }, 3, TimeUnit.MILLISECONDS);

        try {
            bothOperationsLatch.await();
        } catch (InterruptedException e) {
            Assert.fail();
        } finally {
            threadPool.shutdown();
        }
    }

    /**
     * Simple testing strategy that fails on every given Xth flush.
     */
    private static class FailingFlushStrategy implements KeyFlushStrategy<Integer, SimpleBufferValue> {

        private final int failOnEveryXthFlush;
        private final AtomicInteger flushCounter = new AtomicInteger();
        private final List<SimpleBufferValue> flushedValues = new ArrayList<>();

        FailingFlushStrategy(int failOnEveryXthFlush) {
            this.failOnEveryXthFlush = failOnEveryXthFlush;
        }

        @Override
        public boolean flushKey(Integer key, List<SimpleBufferValue> values) {
            boolean flushSucceeds = flushCounter.incrementAndGet() % failOnEveryXthFlush != 0; // every Xth flush fails
            if (flushSucceeds) flushedValues.addAll(values);
            return flushSucceeds;
        }

        void verifyFlushedValues(Integer... expectedValues) {
            for (int idx = 0; idx < expectedValues.length; idx++) {
                Integer expectedValue = expectedValues[idx];
                Integer presentValue = flushedValues.get(idx).getId();
                Assert.assertEquals(expectedValue, presentValue);
            }
        }
    }
}
