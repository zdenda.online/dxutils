package cz.d1x.dxutils.buffer;

import org.junit.Assert;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * <p>
 * Class that helps with complex testing scenario of {@link Buffer} implementations.
 * To use this test, simply create and instance (that requires factory for tested buffer) and run {@link #checkBuffer()}.
 * This test runs multiple concurrent threads that submit values to the buffer under various keys and waits until all
 * values are flushed (that means if the test does not stop, there is likely a bug in your buffer).
 * </p><p>
 * To debug this (if hangs up), you can put breakpoint into BufferAutoFlush or in this class in flushKey
 * (that is periodically called from BufferAutoFlush).
 * </p>
 */
public class ComplexBufferTest {

    /**
     * Parameters of this test.
     */
    public static class ComplexBufferTestParams {
        public int warmUpOperations = 1000;

        public int putThreads = 1000; // there should be enough threads so test after flushing interval succeeds
        public int autoFlushInterval = 50;
        public boolean autoFlushConstantDelay = false;
        public int testTimeoutSeconds = 60 * 5; // 5mins

        // Generally sleep values allow auto-buffer to execute more often
        public int putSleep = 1; // allows to create gaps between multiple put operations
        public int flushSleep = 3; // simulates waiting time to the underlying storage

        public int keysCount = 1000;
        public int putsPerKey = 10;
        public int valuesPerPut = 2;
        public int percentsOfFlushValues = 20; // 2 out of 10 will be flushing (randomized)

        public boolean simulateFlushFailures = true;
        public int percentsOfFlushFailures = 20; // 1 out of 10 flushes will throw runtime exception (randomized)

        public boolean printInfo = false; // print to console all debug info

        @Override
        public String toString() {
            return "putThreads=" + putThreads +
                    "\nautoFlushInterval=" + autoFlushInterval +
                    "\nautoFlushConstantDelay=" + autoFlushConstantDelay +
                    "\nputSleep=" + putSleep +
                    "\nflushSleep=" + flushSleep +
                    "\nkeysCount=" + keysCount +
                    "\nputsPerKey=" + putsPerKey +
                    "\nvaluesPerPut=" + valuesPerPut +
                    "\npercentsOfFlushValues=" + percentsOfFlushValues +
                    "\nsimulateFlushFailures=" + simulateFlushFailures +
                    "\npercentsOfFlushFailures=" + percentsOfFlushFailures;
        }
    }

    private final Random RANDOM = new Random();
    private final List<CountDownLatch> allKeysLatches;
    private final TestKeyFlushStrategy strategy;
    private final Buffer<Integer, TestValue> buffer;
    private final ComplexBufferTestParams testParams;

    public ComplexBufferTest(ComplexBufferTestParams testParams, Function<KeyFlushStrategy<Integer, TestValue>, Buffer<Integer, TestValue>> bufferFactory) {
        this.testParams = testParams;
        this.allKeysLatches = Collections.synchronizedList(new ArrayList<>());
        this.strategy = new TestKeyFlushStrategy(allKeysLatches);
        this.buffer = bufferFactory.apply(strategy);
        this.strategy.init(true);
    }

    public void checkBuffer() {
        warmUp();
        this.strategy.init(false);

        BufferAutoFlush autoFlush = new BufferAutoFlush(buffer, false);
        autoFlush.startFlushing(testParams.autoFlushInterval, TimeUnit.MILLISECONDS, testParams.autoFlushConstantDelay);

        final CountDownLatch allPutsLatch = new CountDownLatch(testParams.keysCount * testParams.putsPerKey);
        Collection<Runnable> puttingThreads = preparePuttingThreads(allPutsLatch);
        ExecutorService putThreadPool = Executors.newFixedThreadPool(testParams.putThreads);
        puttingThreads.forEach(putThreadPool::execute);

        waitForLatches(allPutsLatch);
        autoFlush.stopFlushing();
        putThreadPool.shutdown();

        allKeysLatches.forEach(l -> {
            if (l.getCount() > 0) {
                int key = allKeysLatches.indexOf(l);
                List<TestValue> bufferedValues = buffer.valuesOf(key);
                System.err.println("Key " + key + " didn't flush all values in timeout, in buffer is " + bufferedValues);
                strategy.printKeyHistory(key);
                Assert.fail();
            }
        });

        Assert.assertEquals("Expecting no errors after all values were put and flushed", 0, strategy.errorsCount.get());
        buffer.size().forEach((key, size) -> {
            if (size != 0) strategy.printKeyHistory(key);
            Assert.assertEquals("Expecting empty buffer for key " + key, 0, size.intValue());
        });
    }

    /**
     * Warm-ups JVM by invoking multiple operations.
     */
    private void warmUp() {
        print("Doing warm-up: " + testParams.warmUpOperations + " operations");
        IntStream.range(0, testParams.warmUpOperations).forEach(iteration -> {
            try {
                buffer.put(iteration, new TestValue(iteration, iteration, Math.random() > 0.5));
                buffer.flush();
                buffer.remove(iteration);
            } catch (Exception e) {
                // may be thrown randomly during flush, we don't care
            }
        });
        buffer.clearWithoutFlush();
        Assert.assertTrue(buffer.isClear());
        print("Warm-up finished");
    }

    /**
     * Prepares threads that will simulate concurrent puts into the buffer.
     * There will be always given amount of values for every key but puts will proceed in random order.
     */
    private Collection<Runnable> preparePuttingThreads(CountDownLatch allPutsLatch) {
        List<Runnable> threads = new ArrayList<>();

        AtomicInteger keyGenerator = new AtomicInteger(0);
        IntStream.range(0, testParams.keysCount).forEach(i -> {
            int key = keyGenerator.getAndIncrement();
            allKeysLatches.add(new CountDownLatch(1)); // every key must have separate latch
            IntStream.range(0, testParams.putsPerKey).forEach(batch ->
                    threads.add(() -> {
                        List<TestValue> elements = IntStream.range(0, testParams.valuesPerPut)
                                .mapToObj(idx -> generateElement(batch + 1, idx + 1))
                                .collect(Collectors.toList());
                        sleep(testParams.putSleep);
                        if (Math.random() > 0.7) buffer.size(); // try to call size from time to time
                        try {
                            buffer.put(key, elements.toArray(new TestValue[testParams.valuesPerPut]));
                        } catch (RuntimeException e) {
                            // exception during flush of flushing value but it is in buffer so proceed
                        }
                        allPutsLatch.countDown();
                    }));
        });

        Collections.shuffle(threads); // let's make puts in random order
        return threads;
    }

    /**
     * Waits until all latches are counted-down and prints waiting times.
     */
    private void waitForLatches(CountDownLatch allPutsLatch) {
        long start = System.currentTimeMillis();
        try {
            print("Waiting until put operations are finished for all keys");
            allPutsLatch.await(testParams.testTimeoutSeconds, TimeUnit.SECONDS);
            print("All puts were made in " + (System.currentTimeMillis() - start) + "ms");
            start = System.currentTimeMillis();
            print("Waiting until it is verified that values were flushed for all keys");

            int iteration = 1;
            for (CountDownLatch keysLatch : allKeysLatches) {
                keysLatch.await(testParams.testTimeoutSeconds, TimeUnit.SECONDS);
                if (keysLatch.getCount() > 0) {
                    return;  // I time-outed
                }
                if (iteration % 100 == 0) print("First " + iteration + " keys finished");
                iteration++;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        long totalPutOperations = testParams.keysCount * testParams.putsPerKey;
        long totalValues = testParams.keysCount * testParams.valuesPerPut * testParams.putsPerKey;
        print("All values were flushed in " + (System.currentTimeMillis() - start) + "ms for "
                + totalPutOperations + " put operations (" + totalValues + " values)");
    }

    private TestValue generateElement(int batch, int orderInBatch) {
        boolean invokesFlush = RANDOM.nextInt(100) < testParams.percentsOfFlushValues;
        return new TestValue(batch, orderInBatch, invokesFlush);
    }

    private void print(String msg) {
        if (testParams.printInfo) {
            System.out.println(msg);
        }
    }


    public static class TestValue implements Bufferable {

        final int batch;
        final int orderInBatch;
        private final boolean invokesFlush;

        TestValue(int batch, int orderInBatch, boolean invokesFlush) {
            this.batch = batch;
            this.orderInBatch = orderInBatch;
            this.invokesFlush = invokesFlush;
        }

        @Override
        public boolean invokesFlush() {
            return invokesFlush;
        }

        @Override
        public String toString() {
            return String.valueOf(batch);
        }
    }

    private class TestKeyFlushStrategy implements KeyFlushStrategy<Integer, TestValue> {

        private final List<CountDownLatch> allKeysLatches;
        private Map<Integer, ValuesWithHistory> results;
        private final AtomicInteger errorsCount = new AtomicInteger(0);
        private boolean isWarmUp = true;

        TestKeyFlushStrategy(List<CountDownLatch> allKeysLatches) {
            this.allKeysLatches = allKeysLatches;
        }

        void init(boolean isWarmUp) {
            this.isWarmUp = isWarmUp;
            this.errorsCount.set(0);
            if (buffer.getLockMode() != LockMode.LOCK_BUFFER) {
                results = Collections.synchronizedMap(new HashMap<>());
            } else {
                results = new HashMap<>();
            }
        }

        @Override
        public boolean flushKey(Integer key, List<TestValue> values) {
            ValuesWithHistory actualValues = results.computeIfAbsent(key, k -> new ValuesWithHistory());
            print(key + " flushing " + values + " from original " + actualValues.values);

            if (testParams.simulateFlushFailures) {
                boolean failFlush = RANDOM.nextInt(100) < testParams.percentsOfFlushFailures;
                boolean isExc = Math.random() > 0.5;
                String logMsg = isExc ? "FailE" : "FailF";
                if (failFlush) print(key + " flush will fail " + actualValues.values);
                if (failFlush) actualValues.history.add(logMsg + "(" + values.size() + ")  \t" + values);
                if (failFlush && !isExc) return false; // 50% to return false, 50% to fail with exception
                if (failFlush) throw new RuntimeException("ooops"); // lengthens the test significantly
            }


            actualValues.values.addAll(values);
            List<TestValue> actualVals = actualValues.values;
            int actualSize = actualVals.size();

            print(key + " flushed and now has " + actualSize + ": " + actualVals);
            actualValues.history.add("Added(" + values.size() + ")  \t" + values);
            sleep(testParams.flushSleep);

            if (actualSize == (testParams.putsPerKey * testParams.valuesPerPut)) {
                verifyOrdering(key, actualValues);
                actualValues.history.add("Done(" + values.size() + ")  \t" + values);
                if (!isWarmUp) allKeysLatches.get(key).countDown();
            } else {
                print(key + " flushed but not finished yet, has only " + actualSize + ": " + actualVals);
            }
            return true;
        }

        void printKeyHistory(Integer key) {
            ValuesWithHistory values = results.get(key);
            System.err.println("--> Key " + key + ": \n" + values.historyToString());
        }

        private void verifyOrdering(Integer key, ValuesWithHistory values) {
            if (isWarmUp) return; // doesn't matter during warm-up
            Map<Integer, Integer> previousOrderInBatch = new HashMap<>();
            for (TestValue value : values.values) {
                Integer previousOrder = previousOrderInBatch.get(value.batch);
                if (previousOrder != null && previousOrder >= value.orderInBatch) {
                    System.err.println("--> Key " + key + ": ordering within a batch broken\n" + values.historyToString());
                    errorsCount.incrementAndGet();
                    return;
                }
                previousOrderInBatch.put(value.batch, value.orderInBatch);
            }
        }
    }

    private class ValuesWithHistory {
        final List<String> history;
        final List<TestValue> values;

        ValuesWithHistory() {
            if (buffer.getLockMode() == LockMode.NO_LOCK) {
                history = Collections.synchronizedList(new ArrayList<>());
                values = Collections.synchronizedList(new ArrayList<>());
            } else {
                history = new ArrayList<>();
                values = new ArrayList<>();
            }
        }

        String historyToString() {
            return String.join("\n", history);
        }
    }

    private void sleep(long millis) {
        if (millis <= 0) return;
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
