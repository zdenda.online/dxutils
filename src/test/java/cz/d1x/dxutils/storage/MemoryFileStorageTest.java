package cz.d1x.dxutils.storage;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;

/**
 * Tests {@link MemoryFileStorage} implementation.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class MemoryFileStorageTest {

    @Test
    public void onlyInMemoryWithDefaultFile() {
        MemoryFileStorage storage = new MemoryFileStorage();

        byte[] bytes = utf8Bytes("These are some nice bytes");
        writeToStorage(storage, bytes);

        assertDataInStorage(storage, bytes);
        assertTempFileExists(false);
    }

    @Test
    public void onlyInMemoryWithCustomFile() {
        File tempFile = prepareCustomTempFile();
        MemoryFileStorage storage = new MemoryFileStorage(tempFile);

        byte[] bytes = utf8Bytes("These are some nice bytes");
        writeToStorage(storage, bytes);

        assertDataInStorage(storage, bytes);
        assertCustomFileExists(tempFile, false);
    }

    @Test
    public void thresholdExceededCreatesCustomFile() {
        File tempFile = prepareCustomTempFile();
        MemoryFileStorage storage = new MemoryFileStorage(10, tempFile);

        byte[] bytes = utf8Bytes("These are some nice bytes");
        writeToStorage(storage, bytes);

        assertDataInStorage(storage, bytes);
        assertCustomFileExists(tempFile, true);
        storage.clear();
    }

    @Test
    public void thresholdExceededCreatesDefaultFile() {
        MemoryFileStorage storage = new MemoryFileStorage();

        byte[] bytes = utf8Bytes("These are some nice bytes");
        writeToStorage(storage, bytes);

        assertDataInStorage(storage, bytes);
        assertTempFileExists(false);
        storage.clear();
    }

    @Test
    public void onlyInMemoryAppendsData() {
        MemoryFileStorage storage = new MemoryFileStorage();

        byte[] bytes = utf8Bytes("These are some");
        writeToStorage(storage, bytes);
        byte[] bytes2 = utf8Bytes(" nice bytes");
        writeToStorage(storage, bytes2);

        assertDataInStorage(storage, utf8Bytes("These are some nice bytes"));
        assertTempFileExists(false);
        storage.clear();
    }

    @Test
    public void thresholdExceededAppendsDataToDefaultFile() {
        MemoryFileStorage storage = new MemoryFileStorage(5);

        byte[] bytes = utf8Bytes("These are some");
        writeToStorage(storage, bytes);
        byte[] bytes2 = utf8Bytes(" nice bytes");
        writeToStorage(storage, bytes2);

        assertDataInStorage(storage, utf8Bytes("These are some nice bytes"));
        assertTempFileExists(true);
        storage.clear();
    }

    @Test
    public void thresholdExceededCreatesDirectoryStructureLazily() {
        File testDir = new File(System.getProperty("java.io.tmpdir"), "abc");
        File testFile = new File(testDir, "abc.txt");
        MemoryFileStorage storage = new MemoryFileStorage(10, testFile);

        Assert.assertFalse(testDir.exists());
        storage.write("This is something that exceeds threshold");

        assertDataInStorage(storage, utf8Bytes("This is something that exceeds threshold"));

        storage.clear();
        testDir.delete();
    }

    @Test
    public void writeBytes() {
        MemoryFileStorage storage = new MemoryFileStorage();

        storage.write("Hello".getBytes());

        assertDataInStorage(storage, "Hello".getBytes());
        assertTempFileExists(false);
        storage.clear();
    }

    @Test
    public void writeUtf8String() {
        MemoryFileStorage storage = new MemoryFileStorage();

        storage.write("Hello");

        assertDataInStorage(storage, utf8Bytes("Hello"));
        assertTempFileExists(false);
        storage.clear();
    }

    @Test
    public void readString() {
        MemoryFileStorage storage = new MemoryFileStorage();

        storage.write("Hello");
        String str = storage.readString();

        Assert.assertEquals("Hello", str);
        assertTempFileExists(false);
        storage.clear();
    }

    @Test
    public void sizeIsCorrectInMemory() {
        MemoryFileStorage storage = new MemoryFileStorage();

        storage.write("Hello");
        Assert.assertEquals(5, storage.getSize());

        storage.clear();
    }

    @Test
    public void sizeIsCorrectInFile() {
        MemoryFileStorage storage = new MemoryFileStorage(5);

        storage.write("This is over limit");
        Assert.assertEquals(18, storage.getSize());

        storage.clear();
    }

    @Test
    public void autocloseDestroysFile() {
        File tempFile = prepareCustomTempFile();
        try (MemoryFileStorage storage = new MemoryFileStorage(10, tempFile)) {
            byte[] bytes = utf8Bytes("These are some nice bytes");
            writeToStorage(storage, bytes);
            assertCustomFileExists(tempFile, true);
        }
        assertCustomFileExists(tempFile, false); // should be destroyed by autoclose
    }

    @Test
    public void equalsTrueForSameInMemoryData() {
        MemoryFileStorage storage = new MemoryFileStorage();
        MemoryFileStorage storage2 = new MemoryFileStorage();

        byte[] bytes = utf8Bytes("These are some nice bytes");
        byte[] bytes2 = utf8Bytes("These are some nice bytes");

        writeToStorage(storage, bytes);
        writeToStorage(storage2, bytes2);

        Assert.assertTrue(storage.equals(storage2));
    }

    @Test
    public void equalsFalseForDifferentInMemoryData() {
        MemoryFileStorage storage = new MemoryFileStorage();
        MemoryFileStorage storage2 = new MemoryFileStorage();

        byte[] bytes = utf8Bytes("These are some nice bytes");
        byte[] bytes2 = utf8Bytes("These are some other bytes");

        writeToStorage(storage, bytes);
        writeToStorage(storage2, bytes2);

        Assert.assertFalse(storage.equals(storage2));
    }

    @Test
    public void equalsTrueForSameFileBackedData() {
        MemoryFileStorage storage = new MemoryFileStorage(5);
        MemoryFileStorage storage2 = new MemoryFileStorage(5);

        byte[] bytes = utf8Bytes("These are some nice bytes");
        byte[] bytes2 = utf8Bytes("These are some nice bytes");

        writeToStorage(storage, bytes);
        writeToStorage(storage2, bytes2);

        Assert.assertTrue(storage.equals(storage2));
        storage.clear();
        storage2.clear();
    }

    @Test
    public void equalsFalseForDifferentFileBackedData() {
        MemoryFileStorage storage = new MemoryFileStorage();
        MemoryFileStorage storage2 = new MemoryFileStorage();

        byte[] bytes = utf8Bytes("These are some nice bytes");
        byte[] bytes2 = utf8Bytes("These are some other bytes");

        writeToStorage(storage, bytes);
        writeToStorage(storage2, bytes2);

        Assert.assertFalse(storage.equals(storage2));
        storage.clear();
        storage2.clear();
    }

    private void assertCustomFileExists(File file, boolean exists) {
        Assert.assertTrue((exists && file.exists()) || (!exists && !file.exists()));
    }

    private void assertTempFileExists(boolean exists) {
        File[] files = new File(System.getProperty("java.io.tmpdir")).listFiles();
        Assert.assertNotNull(files);
        for (File f : files) {
            if (f.getName().contains("memory_file")) {
                if (exists) {
                    return; // it should exist and is there!
                } else {
                    Assert.fail("memory_file should not exist in temp but " + f.getAbsolutePath() + " found!");
                }
            }
        }
    }

    private void assertDataInStorage(DataStorage storage, byte[] expectedData) {
        byte[] actualData = readFromStorage(storage);
        Assert.assertArrayEquals(expectedData, actualData);
    }

    private void writeToStorage(DataStorage storage, byte[] data) {
        OutputStream os = storage.getOutputStream();
        try {
            os.write(data);
        } catch (IOException e) {
            Assert.fail("IOException thrown " + e.getMessage());
        } finally {
            try {
                os.close();
            } catch (IOException e) {
                Assert.fail("IOException thrown " + e.getMessage());
            }
        }
    }

    private byte[] readFromStorage(DataStorage storage) {
        InputStream is = storage.getInputStream();
        try {
            return IOUtils.toByteArray(is);
        } catch (IOException e) {
            Assert.fail("IOException thrown " + e.getMessage());
            return new byte[0]; // satisfy compiler
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                Assert.fail("IOException thrown " + e.getMessage());
            }
        }
    }

    private File prepareCustomTempFile() {
        try {
            File f = File.createTempFile("data_storage_test", null);
            boolean deleted = f.delete();
            if (!deleted) System.out.println("File was not deleted");
            return f;
        } catch (IOException e) {
            Assert.fail("IOException " + e.getMessage());
            return new File("."); // satisfy compiler
        }
    }

    private byte[] utf8Bytes(String str) {
        try {
            return str.getBytes(Charsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
